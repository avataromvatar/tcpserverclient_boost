/*
 * example1.cpp
 *
 *  Created on: Aug 16, 2020
 *      Author: avatar
 */

#include "ServerTCP.hpp"
#include "ClientTCP.hpp"
#include <vector>

using namespace std;

vector<boost::shared_ptr<ClientTCP>> _example1_ServerClient;
vector<boost::shared_ptr<ClientTCP>> _example1_Client;
void connection_complete(const boost::system::error_code &err);

#define EXAMPLE1_NUM_CLIENT 10
int _example1_count = 0;
int _example1_step = 1;
char _example1_c[100]={0};
int example1() {

	_example1_count = 0;
	_example1_step = 1;
	memset(_example1_c,0,sizeof(_example1_c));

	//Стартуем Сервер
	boost::shared_ptr<ServerTCP> server(new ServerTCP(34222));
	server->start( [](boost::shared_ptr<ClientTCP> ptr) {
		printf("Server Connection %s :%d\n",ptr.get()->getAdress().data(),ptr.get()->getPort());
		_example1_ServerClient.push_back(ptr);
		_example1_step++;
//		_example1_count++;
	});
	//Стартуем Клиента
	ClientTCP client;
	client.connect("127.0.0.1",34222, connection_complete);

	while(_example1_step==1);
	while(_example1_count==0);//wait connection_complete
	printf("STATE %d  count %d \n\n",_example1_step,_example1_count);
		_example1_ServerClient[0].get()->read_async(_example1_c, 100, [](const boost::system::error_code & err,int len){
				if(_example1_c[0]=='!')
					_example1_step ++;
				printf("2 read data %s len=%d\n",_example1_c,len);
				_example1_count++;
			});
			client.write_async("!AA", 3, [](const boost::system::error_code & err,int len){
				printf("2 write data end len=%d\n",len);
				_example1_step++;
			});
	while(_example1_step==2 || _example1_step==3 );
	while(_example1_count==1); //wait read_async
	printf("STATE %d  count %d \n\n",_example1_step,_example1_count);
		client.read_async(_example1_c, 100, [](const boost::system::error_code & err,int len){
			if(_example1_c[0]=='O')
				_example1_step ++;
			printf("read data %s len=%d\n",_example1_c,len);
			_example1_count++;
		});
		_example1_ServerClient[0].get()->write_async("OK", 2, [](const boost::system::error_code & err,int len){
			printf("write data end len=%d\n",len);
			_example1_step++;
		});



		while(_example1_step==4 || _example1_step==5 );
		while(_example1_count==2);// wait _example1_ServerClient[0].get()->read_async
	printf("STATE %d  count %d \n\n",_example1_step,_example1_count);
	//Останавливаем сервер
	server.get()->stop();
	//Если там были клиенты сервера они удалятся так как использована shared_ptr
	_example1_ServerClient.clear();
	_example1_Client.clear();
		if(_example1_step == 6)
			return 0;
	return _example1_step;
}

void connection_complete(const boost::system::error_code &err) {
	_example1_count++;
	printf("connect count %d err=%d\n", _example1_count, err.value());
}

