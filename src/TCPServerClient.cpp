

#include <iostream>
#include "stdio.h"
#include "unistd.h"
using namespace std;

extern int example1();
int main() {
	int ret = 0;
	printf("Begin run example\n");
	for(int i=0;i<5;i++)
	{
		ret = example1();
		usleep(1000);
	printf("EXAMPLE 1 %d -> %s\n",i,ret==0?"OK":"ERROR");
	if(ret)
		return 0;
	}
	return 0;
}
