/*
 * ClientTCP.hpp
 *
 *  Created on: 11 Р°РІРі. 2020 Рі.
 *      Author: avatar
 */

#ifndef CLIENTTCP_HPP_
#define CLIENTTCP_HPP_

#include <boost/bind/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>
#include "vector"



//#define WEBSOCKET_ON
#ifdef WEBSOCKET_ON
#include <boost/beast.hpp>
//#include <boost/beast/websocket.hpp>
//#include <boost/beast/core.hpp>
#endif


//using namespace boost::asio;


/**
 * TCP РљР»РёРµРЅС‚ РёСЃРїРѕР»СЊР·СѓРµС‚СЃСЏ РґР»СЏ РїРѕРґРєР»СЋС‡РµРЅРёСЏ Рє СѓРґР°Р»РµРЅРЅРѕРјСѓ TCP РЎРµСЂРІРµСЂСѓ.
 * РўР°Рє Р¶Рµ СЌС‚РѕС‚ РєР»Р°СЃСЃ РёСЃРїРѕР»СЊР·СѓРµС‚СЃСЏ РґР»СЏ С…СЂР°РЅРµРЅРёСЏ РєР»РёРµРЅС‚РѕРІ РїРѕРґРєР»СЋС‡РµРЅРЅС‹С…
 * Рє СЃРµСЂРІРµСЂСѓ (РЅР° СЃС‚РѕСЂРѕРЅРµ СЃРµСЂРІРµСЂР°)
 *
 * РљР»РёРµРЅС‚ СЃРѕРєРµС‚ РѕСЃСѓС‰РµСЃС‚РІР»СЏРµС‚ С‚РѕР»СЊРєРѕ РѕРґРЅРѕ РґРµР№СЃС‚РІРёСЏ РІРѕ РІСЂРµРјРµРЅРё Р»РёР±Рѕ С‡С‚РµРЅРёРµ Р»РёР±Рѕ Р·Р°РїРёСЃСЊ.
 * Р—Р°РїРёСЃСЊ РёРјРµРµС‚ РїСЂРµРѕСЂРёС‚РµС‚ РІС‹С€Рµ С‡РµРј С‡С‚РµРЅРёРµ
 *
 *FIXIT Win BullShit !!! :)
 *
 * If msg cant send to server/client its was send again and again Yhahahahaha TODO add count repeat to write msg
 */
class ClientTCP{
public:

	typedef boost::function<void (const boost::system::error_code)> cb_on_connect;
//	typedef void (*cb_on_connect)(const boost::system::error_code& err );
	/**
	 * Р¤СѓРЅРєС†РёСЏ РѕР±СЂР°С‚РЅРѕРіРѕ РІС‹Р·РѕРІР° РїСЂРё РѕРєРѕРЅС‡Р°РЅРё РїРµСЂРµРґР°С‡Рё РґР°РЅРЅС‹С…
	 * @param err РµСЃР»Рё 0 С‚Рѕ РІСЃРµ С…РѕСЂРѕС€Рѕ РёРЅР°С‡Рµ СЃРјРѕС‚СЂРёС‚Рµ boost::system::errc
	 * @param len РєРѕР»-РІРѕ РґР°РЅРЅС‹С… РѕС‚РїСЂР°РІР»РµРЅРЅС‹С…
	 */
	typedef boost::function<void (const boost::system::error_code,int)> cb_on_write;
//	typedef void (*cb_on_write)(const boost::system::error_code & err,int len);
	/**
	 * Р¤СѓРЅРєС†РёСЏ РѕР±СЂР°С‚РЅРѕРіРѕ РІС‹Р·РѕРІР° РїСЂРё РѕРєРѕРЅС‡Р°РЅРё С‡С‚РµРЅРёСЏ РґР°РЅРЅС‹С…
	 * @param err err РµСЃР»Рё 0 С‚Рѕ РІСЃРµ С…РѕСЂРѕС€Рѕ РёРЅР°С‡Рµ СЃРјРѕС‚СЂРёС‚Рµ boost::system::errc
	 * @param len РєРѕР»-РІРѕ РїСЂРёРЅСЏС‚С‹С… РґР°РЅРЅС‹С…
	 */
	typedef boost::function<void (const boost::system::error_code,int)> cb_on_read;
//	typedef void (*cb_on_read)(const boost::system::error_code & err,int len);

	/**
	 * РљРѕРЅС‚СЃС‚СЂСѓРєС‚РѕСЂ РµСЃР»Рё СЃРѕРєРµС‚ СѓР¶Рµ СЃРѕР·РґР°РЅ. Р�СЃРїРѕР»СЊР·СѓРµС‚СЃСЏ РЅР° СЃС‚РѕСЂРѕРЅРµ СЃРµСЂРІРµСЂР° РґР»СЏ
	 * С‚РµС… РєР»РёРµРЅС‚РѕРІ РєС‚Рѕ РїРѕРґРєР»СЋС‡РёР»СЃСЏ Рє РЅРµРјСѓ
	 * @param sock РіРѕС‚РѕРІС‹Р№ Рє РёСЃРїРѕР»СЊР·РѕРІР°РЅРёСЋ  СЃРѕРєРµС‚
	 */
	ClientTCP(boost::asio::io_service *ios, boost::asio::ip::tcp::socket *sock);
	/**
	 * РљРѕРЅСЃС‚СЂСѓРєС‚РѕСЂ РїСЂРµРґРЅР°Р·РЅР°С‡РµРЅРЅС‹Р№ РґР»СЏ РїРѕРґРєР»СЋС‡РµРЅРёРµ Рє СЃРµСЂРІРµСЂСѓ
	 * @param adr Р°РґСЂРµСЃ СЃРµСЂРІРµСЂР° "127.0.0.1" РЅР°РїСЂРёРјРµСЂ
	 * @param port РЅРѕРјРµСЂ РїРѕСЂС‚Р° СЃРµСЂРІРµСЂР°
	 */
	ClientTCP(const char *adr,int port);
//	ClientTCP(boost::asio::io_service *ios);

	ClientTCP();
	virtual ~ClientTCP();
	enum eState {
		CLOSED=1, WAIT_CONNECTION,CONNECTION,WORK, STATE_ERROR,
	};
	boost::asio::io_service *getIOService(){return ios;}
	boost::asio::ip::tcp::socket *getSocket(){return sock;}
	eState getState() {return state;}
	bool isServerSide(){return flag_isServerSide;}
	bool isRun(){return _isRun;}
	/**
	 *
	 * @param adr
	 * @param port
	 * @return
	 */
	int connect(const char *adr,int port,cb_on_connect cb);
	//Р—Р°РїСѓСЃРєР°РµС‚ РѕС‚РґРµР»СЊРЅС‹Р№ РїРѕС‚РѕРє РґР»СЏ РєР»РёРµРЅС‚Р°
	//int start();
	///Р—Р°РєСЂС‹РІР°РµС‚ РїРѕС‚РѕРє Рё РєР»РёРµРЅС‚СЃРєРёР№ СЃРѕРєРµС‚
	void close();
	///Р¤СѓРЅРєС†РёСЏ РїРѕР»СѓС‡РµРЅРёСЏ Р°РґСЂРµСЃР° РІ РІРёРґРµ СЃС‚СЂРѕРєРё
	std::string getAdress(){return ep.address().to_string();}
	///Р¤СѓРЅРєС†РёСЏ РїРѕР»СѓС‡РµРЅРёСЏ РїРѕСЂС‚Р° СЃРµСЂРІРµСЂР°
	int getPort(){return ep.port();}
	/**
	 * РђСЃРёРЅС…СЂРѕРЅРЅР°СЏ РѕС‚РїСЂР°РІРєР° РґР°РЅРЅС‹С…
	 * @param data СѓРєР°Р·Р°С‚РµР»СЊ РЅР° РґР°РЅРЅС‹Рµ
	 * @param len РґР»РёРЅРЅР° РґР°РЅРЅС‹С…
	 * @param cb_write С„СѓРЅРєС†РёСЏ РѕР±СЂР°С‚РЅРѕРіРѕ РІС‹Р·РѕРІР°
	 * @return РІСЃРµРіРґР° 0 :)
	 */
	int write_async(char *data, int len,cb_on_write cb_write,int timeOut_msec=500);
	/**
	 * РђСЃРёРЅС…СЂРѕРЅРЅРѕРµ С‡С‚РµРЅРёРµ РґР°РЅРЅС‹С…
	 * @param data СѓРєР°Р·Р°С‚РµР»СЊ РЅР° РјР°СЃСЃРёРІ
	 * @param len СЂР°Р·РјРµСЂ РјР°СЃСЃРёРІР°
	 * @param cb_read С„СѓРЅРєС†РёСЏ РѕР±СЂР°С‚РЅРѕРіРѕ РІС‹С…РѕРІР°
	 * @return РІСЃРµРіРґР° 0 :)
	 */
	int read_async(char *data, int len,cb_on_read cb_read,int timeOut_msec=500);
private:

	///Р”Р»СЏ РјР°С€РёРЅС‹ СЃРѕСЃС‚РѕСЏРЅРёР№
	enum eStateSocket{
		WAIT,
		WRITE_BEGIN,
		WRITE_WAIT_END,
		WRITE_END,
		READ_BEGIN,
		READ_WAIT_END,
		READ_END,
	};

	///Р’РЅСѓС‚СЂРµРЅРЅРёР№ РєРѕРЅС‚РµР№РЅРµСЂ РґР»СЏ С…СЂР°РЅРµРЅРёСЏ Р·Р°РїСЂРѕСЃРѕРІ РЅР° Р·Р°РїРёСЃСЊ
		struct _toConnectContiner
		{
			ClientTCP::cb_on_connect  cb=0;
			char flag=0;
		};
	///Р’РЅСѓС‚СЂРµРЅРЅРёР№ РєРѕРЅС‚РµР№РЅРµСЂ РґР»СЏ С…СЂР°РЅРµРЅРёСЏ Р·Р°РїСЂРѕСЃРѕРІ РЅР° Р·Р°РїРёСЃСЊ
	struct _toWriteContiner
	{
		boost::shared_ptr<std::vector<char>> buf;
//		boost::asio::mutable_buffer buf;
		ClientTCP::cb_on_write  cb;
		boost::asio::deadline_timer timer;
		char flag_send_ok=0;
		_toWriteContiner(boost::shared_ptr<std::vector<char>> buf,ClientTCP::cb_on_write  cb,boost::asio::deadline_timer timer1)
		:timer(std::move(timer1))
		{
			this->buf = buf;
			this->cb = cb;
			flag_send_ok = 0;
		}
	};
	///Р’РЅСѓС‚СЂРµРЅРЅРёР№ РєРѕРЅС‚РµР№РЅРµСЂ РґР»СЏ С…СЂР°РЅРµРЅРёСЏ Р·Р°РїСЂРѕСЃРѕРІ РЅР° С‡С‚РµРЅРёРµ
	struct _toReadContiner
	{
		boost::asio::mutable_buffer buf;
		ClientTCP::cb_on_read  cb;
		boost::asio::deadline_timer timer;
		_toReadContiner(boost::asio::mutable_buffer buf,ClientTCP::cb_on_read  cb,boost::asio::deadline_timer timer1)
		:timer(std::move(timer1)){
			this->buf = buf;
			this->cb = cb;
//			this->timer(timer);
		}
	};

	void run();
	void write_complete(const boost::system::error_code & err,std::size_t _len,ClientTCP *i);
	void read_complete( const boost::system::error_code & err,std::size_t _len);
	void connection_complete(const boost::system::error_code& err);
	void deadline_timer(const boost::system::error_code& err);
	void deadline_timer_write(const boost::system::error_code& err);

	bool flag_isServerSide=false;
	eState state = CLOSED;
	bool _isRun= false;
	boost::thread thread;
	boost::mutex mutex;
	std::vector<_toWriteContiner> toWrite;
	std::vector<_toReadContiner> toRead;
	_toConnectContiner connectNeed;
	eStateSocket socketState = WAIT;
#ifdef WEBSOCKET_ON
	boost::beast::websocket::stream<boost::asio::ip::tcp::socket&> *ws;
	void handshake_complete(const boost::system::error_code &err);
#endif

	boost::asio::ip::tcp::socket *sock;
	boost::asio::io_service *ios;
	boost::asio::ip::tcp::endpoint ep;


};

#endif /* CLIENTTCP_HPP_ */
