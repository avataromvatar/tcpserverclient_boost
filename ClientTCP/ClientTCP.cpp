/*
 * ClientTCP.cpp
 *
 *  Created on: 11 авг. 2020 г.
 *      Author: avatar
 */

#include "ClientTCP.hpp"
#include "stdio.h"

#include "boost/chrono.hpp"
#include "boost/locale.hpp"

#include "time.h"
#include <sys/time.h>

using namespace std;
using namespace boost::asio;
using namespace boost::locale;

void __printCurrTime() {
	int len = 0;
	time_t rawtime;
	struct tm *timeinfo;
	struct timeval tv;

	time(&rawtime);                               // текущая дата в секундах
	timeinfo = localtime(&rawtime); // текущее локальное время, представленное в структуре

	gettimeofday(&tv, 0);
	char c[100] = { 0 };
	len += strftime((c + len), 100, "%H:%M:%S ", timeinfo); // форматируем строку времени
	len += sprintf((c + len), "%d ", (tv.tv_usec / 1000)); //добавили мсек
	printf("%s: ", c);

}

ClientTCP::ClientTCP(boost::asio::io_service *ios,
		boost::asio::ip::tcp::socket *sock) :
		ios(ios), /*sock(sock),*/ep(sock->remote_endpoint()) {
	// TODO Auto-generated constructor stub
	state = WORK;
	this->sock = sock;

	flag_isServerSide = true;
	_isRun = true;
	thread = boost::thread([this] {run();});

}

ClientTCP::ClientTCP(const char *adr, int port) :
		ios(new boost::asio::io_service()), ep(ip::address::from_string(adr),
				port) {
//	state = CLOSED;

	sock = new boost::asio::ip::tcp::socket(*ios);
	sock->async_connect(ep,
			boost::bind(&ClientTCP::connection_complete, this,
					boost::placeholders::_1));

	state = CONNECTION;
	_isRun = true;
	thread = boost::thread([this] {run();});
}

ClientTCP::ClientTCP() {
//	flag_iosMy = 1;
	ios = new boost::asio::io_service();
	sock = NULL;
	state = WAIT_CONNECTION;
	thread = boost::thread([this] {run();});
}
ClientTCP::~ClientTCP() {
	// TODO Auto-generated destructor stub

	if (sock)
		printf("%s %s %d BEGI DIE  ....",
				isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
				ep.address().to_string().data(), ep.port());

	ios->stop();
	close();
	state = CLOSED;

	printf("and DIE\n");
	delete ios;
}

int ClientTCP::connect(const char *adr, int port, cb_on_connect cb) {

	if (state == CLOSED) {
		state = WAIT_CONNECTION;
		thread = boost::thread([this] {run();});
	}

	if (state == WAIT_CONNECTION) {

		state = CONNECTION;
		if (sock)
		sock->close();
		sock = new boost::asio::ip::tcp::socket(*ios);
		ep = boost::asio::ip::tcp::endpoint(ip::address::from_string(adr),
		port);

		connectNeed.cb = cb;
		ios->reset();
		sock->async_connect(ep,
		boost::bind(&ClientTCP::connection_complete, this,
				boost::placeholders::_1));
		ios->run_one();
		return 0;
	}

	return 1;
}

void ClientTCP::close() {
	if (state == WORK || CONNECTION || WAIT_CONNECTION) {
		__printCurrTime();
		printf("%s %s %d thread %d CLOSED by thread %d \n",
				isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
				ep.address().to_string().data(), ep.port(), thread.get_id(),
				boost::this_thread::get_id());
		state = WAIT_CONNECTION;
	}
	if (ios)
		ios->stop();

	if (sock) {

		boost::system::error_code ec;

		sock->shutdown(boost::asio::ip::tcp::socket::shutdown_send, ec);
		sock->close(ec);
		delete sock;

//		sock = 0;
	}
	state = CLOSED;
	if (_isRun && boost::this_thread::get_id() != thread.get_id()) {
		thread.join();
	}

//	connectNeed.flag = 0;
}

int ClientTCP::write_async(char *data, int len, cb_on_write cb_write,
		int timeOut_msec) {

	if (state == WORK) {
//	boost::mutex::scoped_lock lock(mutex);
		boost::lock_guard<boost::mutex> lock(mutex);
		boost::shared_ptr<std::vector<char>> tmp(new std::vector<char>(len));

		memcpy(tmp.get()->data(), data, len);
//		toWrite.push_back( { buf:tmp, cb:cb_write });
		toWrite.push_back(
				_toWriteContiner(tmp, cb_write,
						boost::asio::deadline_timer(*ios,
								boost::posix_time::milliseconds(
										timeOut_msec))));
		__printCurrTime();
		printf(
				"%s TASK TCP TO WRITE %s %d socketState=%d toWriteLen:%d ShPtr:%d\n",
				isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
				ep.address().to_string().data(), ep.port(), socketState,
				toWrite.size(), tmp.use_count());
		return 0;
	} else
		return 1;
}

int ClientTCP::read_async(char *data, int len, cb_on_read cb_read,
		int timeOut_msec) {
	if (state == WORK) {
//	boost::mutex::scoped_lock lock(mutex);
		boost::lock_guard<boost::mutex> lock(mutex);
//		printf("%s NEED READ  %s %d socketState=%d\n",
//					isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
//					ep.address().to_string().data(), ep.port(),socketState);
//	toRead.push_back( { buf:boost::asio::buffer(data, len), cb:cb_read ,timer:new boost::asio::deadline_timer(timeOut_msec)});
		toRead.push_back(
				_toReadContiner(boost::asio::buffer(data, len), cb_read,
						boost::asio::deadline_timer(*ios,
								boost::posix_time::milliseconds(
										timeOut_msec))));
//		toRead.push_back(
//						{ buf:boost::asio::buffer(data, len), cb:cb_read, timer
//								:boost::asio::deadline_timer(*ios,
//										boost::posix_time::milliseconds(timeOut_msec)) });

		return 0;
	} else
		return 1;
}

void ClientTCP::run() {
	boost::system::error_code ec;
	int ret = 0;
//	printf("%s THREAD START  %s %d and START \n",
//			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
//			ep.address().to_string().data(), ep.port());

	while (state != CLOSED) {

		if (state == CONNECTION) {
			ret = ios->run(ec);
		}
		if (state == WORK) {
			if (sock->is_open()) {

//				printf("%s %s:%d STATE %d \n",
//										isServerSide() ?
//												"ServerSide ClientTCP" : "ClientTCP",
//										ep.address().to_string().data(), ep.port(),socketState);

				if (socketState == WRITE_END) {
					__printCurrTime();
					printf("%s %s:%d WRITE_END needSendElse %d\n",
							isServerSide() ?
									"ServerSide ClientTCP" : "ClientTCP",
							ep.address().to_string().data(), ep.port(),toWrite.size());
					ios->reset();
//				ios->stop();

					boost::lock_guard<boost::mutex> lock(mutex);
					if (toWrite[0].flag_send_ok) {
						toWrite[0].timer.cancel();
						if (toWrite.size() > 0)
						{
							toWrite.erase(toWrite.begin());
						}

					}

					socketState = WAIT;

				} else if (socketState == READ_END) {
					__printCurrTime();
					printf("%s %s:%d READ_END \n",
							isServerSide() ?
									"ServerSide ClientTCP" : "ClientTCP",
							ep.address().to_string().data(), ep.port());

					ios->reset();
//				ios->stop();

					boost::lock_guard<boost::mutex> lock(mutex);
					toRead[0].timer.cancel();
					toRead.erase(toRead.begin());
					socketState = WAIT;
				} else if (socketState == WRITE_BEGIN) {
					__printCurrTime();
					printf("%s %s:%d WRITE_BEGIN \n",
							isServerSide() ?
									"ServerSide ClientTCP" : "ClientTCP",
							ep.address().to_string().data(), ep.port());
					ios->reset();
					socketState = WRITE_WAIT_END;

					sock->async_write_some(
							boost::asio::buffer(toWrite[0].buf.get()->data(),
									toWrite[0].buf.get()->size()),
							boost::bind(&ClientTCP::write_complete, this,
									boost::placeholders::_1,
									boost::placeholders::_2, this));
					toWrite[0].timer.async_wait(
							boost::bind(&ClientTCP::deadline_timer_write, this,
									boost::placeholders::_1));

//			ret =ios->poll_one(ec);
					ret = ios->run(ec);
				} else if (socketState == READ_BEGIN) {

					ios->reset();
					socketState = READ_WAIT_END;
					__printCurrTime();
					printf("%s %s:%d READ_BEGIN toRead size=%d\n",
							isServerSide() ?
									"ServerSide ClientTCP" : "ClientTCP",
							ep.address().to_string().data(), ep.port(),
							toRead.size());

					sock->async_read_some(toRead[0].buf,
							boost::bind(&ClientTCP::read_complete, this,
									boost::placeholders::_1,
									boost::placeholders::_2));

					toRead[0].timer.async_wait(
							boost::bind(&ClientTCP::deadline_timer, this,
									boost::placeholders::_1));
//			ret = ios->poll_one(ec);
					ret = ios->run(ec);
				} else if (socketState == WAIT && socketState != WRITE_WAIT_END && socketState != READ_WAIT_END) {
					if (toWrite.size() > 0) {
						socketState = WRITE_BEGIN;
					} else if (toRead.size() > 0) {
						socketState = READ_BEGIN;
					} else
//					usleep(500);
						boost::this_thread::sleep_for(
								boost::chrono::microseconds(100));

				}
//			ret = ios->run(ec);
			} else {
				printf("%s %s:%d SOCKET CLOSED  \n",
						isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
						ep.address().to_string().data(), ep.port());
			}
			if (state == WAIT_CONNECTION) {
//			usleep(1000);
				boost::this_thread::sleep_for(
						boost::chrono::microseconds(1000));
//			std::this_thread::sleep_for(1000us);
			}

		}

	}

	_isRun = false;
	printf("%s THREAD STOP  %s %d  \n",
			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
			ep.address().to_string().data(), ep.port());

}

void ClientTCP::connection_complete(const boost::system::error_code &err) {
	if (sock->is_open()) {
//		connectNeed.flag = 2;
		state = WORK;
		__printCurrTime();
		printf("%s connect %s %d and START \n",
				isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
				ep.address().to_string().data(), ep.port());
		if (connectNeed.cb)
			connectNeed.cb(err);

	} else {

		printf("%s connect FALL \n",
				isServerSide() ? "ServerSide ClientTCP" : "ClientTCP");
		close();
	}
	if (connectNeed.cb) {
		connectNeed.cb(err);
		connectNeed.cb = 0;
	}
}

void ClientTCP::write_complete(const boost::system::error_code &err,
		std::size_t _len, ClientTCP *i) {
	__printCurrTime();
	printf("%s %s %d  SEND len:%d ERR:%d i=%d  (%d %d)\n",
			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
			ep.address().to_string().data(), ep.port(), _len, err.value(),
			i->getPort(), toWrite.size(), toWrite[0].cb);

	if (err == boost::system::errc::success)
		toWrite[0].flag_send_ok = 1;
	if (toWrite[0].cb)
		toWrite[0].cb(err, _len);

	if(socketState == WRITE_WAIT_END)
			{
	socketState = WRITE_END;
	ios->stop();

	toWrite[0].timer.cancel();
		}
}

void ClientTCP::read_complete(const boost::system::error_code &err,
		std::size_t _len) {
	__printCurrTime();
	printf("%s %s %d  READ len:%d  ERR:%d SocState:%d\n",
			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
			ep.address().to_string().data(), ep.port(), _len, err.value(),
			socketState);

	if (toRead[0].cb)
		toRead[0].cb(err, _len);

	if(socketState == READ_WAIT_END)
		{
		socketState = READ_END;
	toRead[0].timer.cancel();
	ios->stop();
	}

}
void ClientTCP::deadline_timer(const boost::system::error_code &err) {
	__printCurrTime();
	printf("%s %s %d  READ TIMEOUT socState %d  ERR:%d \n",
			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
			ep.address().to_string().data(), ep.port(), socketState,
			err.value());
	if (socketState == READ_WAIT_END) {
		if (err.value() == boost::system::errc::success) {
			//Если будет не 0 то это отмена
			if (toRead[0].cb) {
				boost::system::error_code er(boost::system::errc::timed_out,
						boost::system::system_category());

				toRead[0].cb(er, 0);
			}

			socketState = READ_END;
			ios->stop();
		}
	}
}
void ClientTCP::deadline_timer_write(const boost::system::error_code &err) {
	__printCurrTime();
	printf("%s %s %d  WRITE TIMEOUT socState %d  ERR:%d \n",
			isServerSide() ? "ServerSide ClientTCP" : "ClientTCP",
			ep.address().to_string().data(), ep.port(), socketState,
			err.value());
	if (socketState == WRITE_WAIT_END) {
		if (err.value() == boost::system::errc::success) {
			//Если будет не 0 то это отмена
			if (toWrite[0].cb) {
				boost::system::error_code er(boost::system::errc::timed_out,
						boost::system::system_category());

				toWrite[0].cb(er, 0);
			}
			toWrite[0].flag_send_ok = 0;
			socketState = WRITE_END;
			ios->stop();
		}
	}
}
