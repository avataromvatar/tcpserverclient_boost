/*
 * ServerTCP.cpp
 *
 *  Created on: 10 авг. 2020 г.
 *      Author: avatar
 */

#include "ServerTCP.hpp"
#include "stdio.h"
using namespace std;
using namespace boost::asio;



ServerTCP::ServerTCP(short port) :
		port(port), ep(ip::tcp::v4(), port) {
//	ep =new ip::tcp::endpoint(ip::tcp::v4(),port);
//	socket = new ip::tcp::socket(ios);
	acceptor = new ip::tcp::acceptor(ios, ep);
	printf("ServerTCP %d Created \n",port);
	// TODO Auto-generated constructor stub
//	acceptor.async_accept(context, handler, )

}

ServerTCP::~ServerTCP() {

	stop();
	if(_ios_master)
	{
		_ios_master->stop();
		delete _ios_master;
	}
	if(client_sock)
	{
//		if(client_sock->is_open())
//		{
//
//			client_sock->shutdown(boost::asio::ip::tcp::socket::shutdown_send);
//			client_sock->close();
//		}
		delete client_sock;
	}
	if (acceptor)
	{
		acceptor->close();
		delete acceptor;
	}
}

int ServerTCP::start(cb_on_connected cb) {
	if (state == DOWN) {
		state =UP;
		printf("ServerTCP Started \n");
//		_ios_master = master;
		_cb_on_connect = cb;
		thread = new boost::thread(&ServerTCP::run,this);
		return 0;
	}
	return 1;
}

int ServerTCP::stop() {
	if (state == UP)
	{
		state = DOWN;
	ios.stop();
	thread->join();
	delete thread;
	}

	return 0;
}

void ServerTCP::run() {

//	if (_ios_master)
	{
		char flag_need_first_client = true;
//		client= new boost::shared_ptr<ClientTCP>(new ClientTCP());
		_ios_master = new boost::asio::io_service();
		client_sock = new ip::tcp::socket(*_ios_master);

		printf("ServerTCP Thread start \n");
		while (state == UP) {
			acceptor->async_accept(*client_sock,
					boost::bind(&ServerTCP::handle_accept,this,client_sock,boost::placeholders::_1));
			 ios.run();
		}
		printf("ServerTCP Thread end \n");
	}
//	else {
//		state = ERROR;
//	}
}

void ServerTCP::handle_accept(ip::tcp::socket *ptr,
		 const boost::system::error_code &err) {
	printf("ServerTCP Client accepted %s %d err=%d \n",ptr->remote_endpoint().address().to_string().data(),ptr->remote_endpoint().port(),err.value());
	if (_cb_on_connect != NULL) {
		auto tmp =  boost::shared_ptr<ClientTCP>(new ClientTCP(_ios_master,client_sock));
		_cb_on_connect(tmp);
		tmp.reset();
//		_ios_master = NULL;
//		client_sock = NULL;
//		if (_ios_master)
		{
			_ios_master = new boost::asio::io_service();
			client_sock = new ip::tcp::socket(*_ios_master);
			acceptor->async_accept(*client_sock,
								boost::bind(&ServerTCP::handle_accept,this,client_sock,boost::placeholders::_1));
		}

	} else {
		state = STATE_ERROR;
		_ios_master = NULL;
		client_sock = NULL;
	}

}
