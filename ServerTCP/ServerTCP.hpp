/*
 * ServerTCP.hpp
 *
 *  Created on: 10 авг. 2020 г.
 *      Author: avatar
 */

#ifndef SERVERTCP_HPP_
#define SERVERTCP_HPP_

#include <boost/bind/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>

#include "ClientTCP.hpp"

/**
 * Сервер не хранит в себе клиентов а передает их внешнему обработчику
 *
 * ios находится всегда в работе так как в обработчики соединения handle_accept каждый раз
 * добовляется задача accept
 */
class ServerTCP {
public:
	/**
	 * Конструктор сервера TCP
	 * @param port номер порта который слушает сервер
	 */
	ServerTCP(short port);
	virtual ~ServerTCP();

	/**
	 * Функция обратного вызова которая будет вызыватся каждый раз когда подключится новый клиент
	 * @param ptr
	 */
	typedef boost::function<void (boost::shared_ptr<ClientTCP> ptr)> cb_on_connected;
//	typedef void (*cb_on_connected)(boost::shared_ptr<ClientTCP> ptr);
	enum eState
	{
		DOWN,
		UP,
		STATE_ERROR,
	};
	/**
	 * Запуск сервера.
	 * @param cb функция обратного вызова
	 * @return 0 если все хорошо
	 */
	int start( cb_on_connected cb);
	/**
	 * Завершает работу сервера
	 * @return
	 */
	int stop();


private:
	eState state = DOWN;

	short port;
	boost::thread *thread;
	boost::asio::io_service ios;
	boost::asio::ip::tcp::endpoint ep;

	boost::asio::ip::tcp::acceptor *acceptor;

	//Client section
	boost::asio::ip::tcp::socket *client_sock;
//	boost::asio::io_service *client;
	cb_on_connected _cb_on_connect;
	boost::asio::io_service *_ios_master;

	void run();
	void handle_accept(boost::asio::ip::tcp::socket *ptr,const boost::system::error_code & err);

};

#endif /* SERVERTCP_HPP_ */
